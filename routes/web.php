<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', 'IndexController@index');
//Route::post('index', 'IndexController@indexAjax');

Route::get('/', [App\Http\Controllers\IndexController::class, 'index']);
Route::post('Index', [App\Http\Controllers\IndexController::class, 'indexAjax']);

Route::post('Companies', [App\Http\Controllers\CompanyController::class, 'index']);
Route::post('CreateCompany', [App\Http\Controllers\CompanyController::class, 'CreateCompany']);
Route::post('SubmitCompany', [App\Http\Controllers\CompanyController::class, 'SubmitCompany']);
Route::post('DeleteCompany', [App\Http\Controllers\CompanyController::class, 'DeleteCompany']);
Route::post('DeleteCompanySubmit', [App\Http\Controllers\CompanyController::class, 'DeleteCompanySubmit']);
Route::post('UpdateCompany', [App\Http\Controllers\CompanyController::class, 'UpdateCompany']);

Route::post('Contacts', [App\Http\Controllers\ContactController::class, 'index']);
Route::post('CreateContact', [App\Http\Controllers\ContactController::class, 'CreateContact']);
Route::post('DeleteContact', [App\Http\Controllers\ContactController::class, 'DeleteContact']);
Route::post('DeleteContactSubmit', [App\Http\Controllers\ContactController::class, 'DeleteContactSubmit']);
Route::post('UpdateContact', [App\Http\Controllers\ContactController::class, 'UpdateContact']);
Route::post('SubmitContact', [App\Http\Controllers\ContactController::class, 'SubmitContact']);
