window.doAjax = function (url, data, afterAjax = null, method = "POST", loaderShow = true) {
    // data.url = url;
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });
    if (loaderShow)
        $("#loader").show();
    $.ajax({
        type: method,
        url: url,
        dataType: "json",
        data: { data },
        success: function (data) {
            if (data.success == "ok") {
                if (afterAjax != null) {
                    afterAjax(data);
                }
            }
        }
    });
}
