/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('contacts', require('./components/pages/contacts.vue').default);
Vue.component('companies', require('./components/pages/companies.vue').default);

Vue.component('vuebutton', require('./components/items/vuebutton.vue').default);
Vue.component('vuenav', require('./components/items/vuenav.vue').default);
Vue.component('vueloading', require('./components/items/vueloading.vue').default);
Vue.component('content-header', require('./components/items/content-header.vue').default);
Vue.component('alert', require('./components/items/alert.vue').default);
Vue.component('paginate', require('./components/items/paginate.vue').default);

Vue.component('create-contact', require('./components/modals/create-contact.vue').default);
Vue.component('create-company', require('./components/modals/create-company.vue').default);
Vue.component('confirm', require('./components/modals/confirm.vue').default);

Vue.component('pagination', require('laravel-vue-pagination'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
