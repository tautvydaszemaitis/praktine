/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
window.doAjax = function (url, data) {
  var afterAjax = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var method = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "POST";
  var loaderShow = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
  // data.url = url;
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });
  if (loaderShow) $("#loader").show();
  $.ajax({
    type: method,
    url: url,
    dataType: "json",
    data: {
      data: data
    },
    success: function success(data) {
      if (data.success == "ok") {
        if (afterAjax != null) {
          afterAjax(data);
        }
      }
    }
  });
};
/******/ })()
;