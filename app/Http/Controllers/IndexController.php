<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $data = null;
        return view('layouts.index')->with('data', $data);
    }

    public function indexAjax(Request $request) {
        $success = 'ok';
        $data = null;
        $view = view("index",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'data' => $data
        ]);
    }
}
