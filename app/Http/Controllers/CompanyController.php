<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyContacts;
use App\Models\Contact;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public $pageName = "Kompanijos";

    public function index(Request $request, $d=null )
    {
        $success = 'ok';
        $data = $d;
        $data['companies'] = Company::paginate(10);
        foreach($data['companies'] as $key => $val)
        {
            $data['companies'][$key]->contactsCount =
            CompanyContacts::where('company_id', '=', $data['companies'][$key]->id)->count();
        }
        $data['companiesCount'] = Company::count();
        $data['pageName'] = $this->pageName;
        $view = view("pages.companies",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'data' => $data
        ]);
    }

    public function SubmitCompany(Request $request)
    {
        $values = $request->all()['data'];
        $errorCode = 0;

        $countContacts = Company::where('email', '=', $values['email'])->count();

        if($values['email'] == null)
        {
            $errorCode = 1;
        }else if($values['name'] == null) {
            $errorCode = 2;
        }else if($countContacts > 0) {
            $errorCode = 3;
        }else{
            $errorCode = 0;
        }

        switch ($errorCode) {
            case 0:
              $create = new Company();
              $create->email = $values['email'];
              $create->name  = $values['name'];
              $create->save();
              $data['alert']['type'] = "success";
              $data['alert']['text'] = "Nauja kompanija sėkmingai sukurta!";
              break;
            case 1:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Email laukas negali būti tuščias!";
              break;
            case 2:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Name laukas negali būti tuščias!";
              break;
            case 3:
                $data['alert']['type'] = "danger";
                $data['alert']['text'] = "Toks email jau egzistuoja!";
              break;
          }

        return $this->index($request, $data);
    }

    public function UpdateCompany(Request $request) {
        $values = $request->all()['data'];

        $allContacts = CompanyContacts::where('company_id', '=', $values['id'])->get();
        foreach($allContacts as $one)
        {
            $one->delete();
        }

        foreach($values['contacts'] as $c)
        {
            $create = new CompanyContacts();
            $create->company_id = $values['id'];
            $create->contact_id = $c;
            $create->save();
        }
        $errorCode = 0;

        if($values['email'] == null)
        {
            $errorCode = 1;
        }else if($values['name'] == null) {
            $errorCode = 2;
        }else{
            $errorCode = 0;
        }

        switch ($errorCode) {
            case 0:
              $create = Company::where('id', '=', $values['id'])->first();
              $create->email = $values['email'];
              $create->name  = $values['name'];
              $create->save();
              $data['alert']['type'] = "success";
              $data['alert']['text'] = "Kontaktas sėkmingai atnaujintas!";
              break;
            case 1:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Email laukas negali būti tuščias!";
              break;
            case 2:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Name laukas negali būti tuščias!";
              break;
          }

        return $this->index($request, $data);
    }

    public function CreateCompany(Request $request)
    {
        $success = 'ok';
        $data = null;
        $data['company'] = null;
        $data['company']['name'] = '';
        $data['company']['email'] = '';
        $data['type'] = 'create';
        $data['companiesCount'] = Company::count();
        $data['id'] = -1;
        if(isset($request->all()['data']["id"]))
        {
            $id = $request->all()['data']["id"];
            $data['company'] = Company::where('id', '=', $id)->first();
            $data['contacts'] = Contact::get();
            foreach($data['contacts'] as $key => $val)
            {
                $count = CompanyContacts::where('company_id', '=', $id)
                ->where('contact_id','=', $data['contacts'][$key]->id)
                ->count();

                $data['contacts'][$key]->selected = $count;
            }
            $data['type'] = 'edit';
            $data['id'] = $id;
        }

        $data['pageName'] = $this->pageName;;
        $data['companies'] = Company::paginate(10);
        $view = view("pages.companies",compact('data'))->render();
        $view2 = view("components.create-company",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'html2' => $view2,
            'data' => $data
        ]);
    }

    public function DeleteCompanySubmit(Request $request)
    {
        $id = $request->all()['data']['id'];
        $d = Company::where('id', '=', $id)->first();
        $d->delete();
        $data['alert']['type'] = "success";
        $data['alert']['text'] = "Įrašas sėkmingai pašalintas!";
        return $this->index($request, $data);
    }

    public function DeleteCompany(Request $request) {
        $success = 'ok';
        $data = null;
        $data['pageName'] = $this->pageName;;
        $data['url'] = "DeleteCompanySubmit";
        $data['urlBack'] = "Companies";
        $data['companies'] = Company::paginate(10);
        $data['id'] = $request->all()['data']["id"];
        $data['conf'] = "Trinti";
        $data['title'] = "Patvirtinimas";
        $data['text'] = "Ar tikrai norite panaikinti įrašą?";
        $data['companiesCount'] = Company::count();
        $view = view("pages.companies",compact('data'))->render();
        $view2 = view("components.confirm",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'html2' => $view2,
            'data' => $data
        ]);
    }
}
