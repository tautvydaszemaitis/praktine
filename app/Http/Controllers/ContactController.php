<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public $pageName = "Kontaktai";

    public function index(Request $request, $d=null)
    {
        $success = 'ok';
        $data = $d;
        $data['pageName'] = $this->pageName;
        $data['contacts'] = Contact::paginate(10);
        //dd($data['contacts'][0]);
        $view = view("pages.contacts",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'data' => $data
        ]);
    }

    public function UpdateContact(Request $request) {
        $values = $request->all()['data'];
        $errorCode = 0;

        if($values['email'] == null)
        {
            $errorCode = 1;
        }else if($values['name'] == null) {
            $errorCode = 2;
        }else{
            $errorCode = 0;
        }

        switch ($errorCode) {
            case 0:
              $create = Contact::where('id', '=', $values['id'])->first();
              $create->email = $values['email'];
              $create->name  = $values['name'];
              $create->save();
              $data['alert']['type'] = "success";
              $data['alert']['text'] = "Kontaktas sėkmingai atnaujintas!";
              break;
            case 1:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Email laukas negali būti tuščias!";
              break;
            case 2:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Name laukas negali būti tuščias!";
              break;
          }

        return $this->index($request, $data);
    }

    public function SubmitContact(Request $request)
    {
        $values = $request->all()['data'];
        $errorCode = 0;

        $countContacts = Contact::where('email', '=', $values['email'])->count();

        if($values['email'] == null)
        {
            $errorCode = 1;
        }else if($values['name'] == null) {
            $errorCode = 2;
        }else if($countContacts > 0) {
            $errorCode = 3;
        }else{
            $errorCode = 0;
        }

        switch ($errorCode) {
            case 0:
              $create = new Contact();
              $create->email = $values['email'];
              $create->name  = $values['name'];
              $create->save();
              $data['alert']['type'] = "success";
              $data['alert']['text'] = "Naujas kontaktas sėkmingai sukurtas!";
              break;
            case 1:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Email laukas negali būti tuščias!";
              break;
            case 2:
              $data['alert']['type'] = "danger";
              $data['alert']['text'] = "Name laukas negali būti tuščias!";
              break;
            case 3:
                $data['alert']['type'] = "danger";
                $data['alert']['text'] = "Toks email kontaktų sąraše jau egzistuoja!";
              break;
          }

        return $this->index($request, $data);
    }

    public function CreateContact(Request $request)
    {
        $success = 'ok';
        $data = null;
        $data['contact'] = null;
        $data['contact']['name'] = '';
        $data['contact']['email'] = '';
        $data['type'] = 'create';
        $data['id'] = -1;
        if(isset($request->all()['data']["id"]))
        {
            $id = $request->all()['data']["id"];
            $data['contact'] = Contact::where('id', '=', $id)->first();
            $data['type'] = 'edit';
            $data['id'] = $id;
        }

        $data['pageName'] = $this->pageName;
        $data['contacts'] = Contact::paginate(10);
        $view = view("pages.contacts",compact('data'))->render();
        $view2 = view("components.create-contact",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'html2' => $view2,
            'data' => $data
        ]);
    }

    public function DeleteContactSubmit(Request $request)
    {
        $id = $request->all()['data']['id'];
        $d = Contact::where('id', '=', $id)->first();
        $d->delete();
        $data['alert']['type'] = "success";
        $data['alert']['text'] = "Įrašas sėkmingai pašalintas!";
        return $this->index($request, $data);
    }

    public function DeleteContact(Request $request) {
        $success = 'ok';
        $data = null;
        $data['pageName'] = $this->pageName;
        $data['url'] = "DeleteContactSubmit";
        $data['urlBack'] = "Contacts";
        $data['contacts'] = Contact::paginate(10);
        $data['id'] = $request->all()['data']["id"];
        $data['conf'] = "Trinti";
        $data['title'] = "Patvirtinimas";
        $data['text'] = "Ar tikrai norite panaikinti įrašą?";
        $view = view("pages.contacts",compact('data'))->render();
        $view2 = view("components.confirm",compact('data'))->render();
        return response()->json([
            'success'=>$success,
            'html' => $view,
            'html2' => $view2,
            'data' => $data
        ]);
    }
}
